package com.example.psethi.weatherforcast;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.psethi.weatherforcast.Services.LocationChangeService;
import com.example.psethi.weatherforcast.Utils.CreateNotification;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

//**import com.google.android.gms.common.ConnectionResult;

public class MainActivity extends AppCompatActivity implements LocationListener {

    private static final int PERMISSION_REQUEST_CODE = 200;

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String[] perms = {"android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION"};

        Log.d("MainActivity","onCreate");
        if(savedInstanceState == null)
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ActivityCompat.requestPermissions(this, perms, PERMISSION_REQUEST_CODE);
            }
        }

        startService(new Intent(this, LocationChangeService.class));

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        /*super.onRequestPermissionsResult(requestCode, permissions, grantResults);*/
        switch (requestCode)
        {
            case PERMISSION_REQUEST_CODE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    String city = LocationHelper.getCityLocation(this);

                    SharedPreferences.Editor editor = getSharedPreferences("APP Storage",MODE_PRIVATE).edit();
                    editor.putString("city",city);
                    editor.apply();

                    Bundle bundle = new Bundle();
                    bundle.putString("city",city);

                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    ForcastFragment fragment = new ForcastFragment();
                    fragment.setArguments(bundle);
                    transaction.replace(R.id.fragment_forcast, fragment);
                    transaction.commit();
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //No call for super(). Bug on API Level > 11.
    }

    @Override
    public void onLocationChanged(Location location) {

        if(location != null) {

            double lat = location.getLatitude();
            double lng = location.getLongitude();

            String cityname = getCityName(lat,lng);

            Snackbar.make(this.findViewById(R.id.fragment_forcast),"Temperature of city: "+cityname,Snackbar.LENGTH_SHORT).show();

            new CreateNotification(this);
        }
    }

    private String getCityName(double lat, double lng) {
        Geocoder gcd=new Geocoder(getBaseContext(), Locale.getDefault());
        List<Address> addresses;
        String cityname = null;

        try {
            addresses=gcd.getFromLocation(lat,lng,1);
            if(addresses.size()>0)
            {
                cityname = addresses.get(0).getLocality().toString();
            }

        } catch (IOException e) {
            e.printStackTrace();

        }
        return cityname;
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

}
