package com.example.psethi.weatherforcast;

import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.psethi.weatherforcast.Beans.ForecastBean;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


/**
 * A placeholder fragment containing a simple view.
 */
public class ForcastFragment extends Fragment
{
    RecycleViewAdapter recycleViewAdapter;
    RecyclerView listForcast;
    SwipeRefreshLayout swipeRefreshLayout;
    private TextView txtTodayTemp;
    private ArrayList<ForecastBean> beanArrayList;
    final String DEGREE  = "\u00b0";
    boolean toFahrenheit;
    private String city;

    public ForcastFragment()
    {

    }

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        new ForcastRequest().execute(city+",in",String.valueOf(toFahrenheit));
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_main,null);

        Log.d("ForcastFragment","I am in!!");
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,android.R.color.holo_green_light,android.R.color.holo_orange_light,android.R.color.holo_blue_dark);
        swipeRefreshLayout.setOnRefreshListener(new SwipeToRefresh());

        listForcast = (RecyclerView) rootView.findViewById(R.id.listview_forcast);
        listForcast.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());

        listForcast.setLayoutManager(layoutManager);

        txtTodayTemp = (TextView) rootView.findViewById(R.id.txt_today_temp);

        city = getArguments().getString("city");

        return rootView;
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater)
    {
        menuInflater.inflate(R.menu.forcast_fragment,menu);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        int id = menuItem.getItemId();
        if(id == R.id.action_refresh)
        {
            new ForcastRequest().execute(city + ",in",String.valueOf(toFahrenheit));
        }
        else if(id == R.id.change_fahrenheit)
        {
            toFahrenheit = true;
            new ForcastRequest().execute(city + ",in",String.valueOf(toFahrenheit));
        }
        else if(id == R.id.change_celcius)
        {
            toFahrenheit = false;
            new ForcastRequest().execute(city + ",in",String.valueOf(toFahrenheit));
        }
        return true;
    }

    public class ForcastRequest extends AsyncTask<String, Void, Void> {

        protected Void doInBackground(String... params) {
            HttpURLConnection urlConnection = null;
            BufferedReader bufferedReader = null;

            String forcastJson = null;
            try {

                final String FORCAST_MAIN_API = "http://api.openweathermap.org/data/2.5/forecast/daily?q=";
                final String API_KEY_AND_MODE_OF_RESPONSE = "&mode=json&cnt=16&appid=83ff5ed46c5fe37d913075db6c7cdbb4";
                //URL url = new URL("http://api.openweathermap.org/data/2.5/forecast?q=Pune,in&mode=json&appid=83ff5ed46c5fe37d913075db6c7cdbb4");
                URL url = new URL(FORCAST_MAIN_API + params[0] + API_KEY_AND_MODE_OF_RESPONSE);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    return null;
                }
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    buffer.append(line);
                }

                if (buffer == null)
                    return null;

                forcastJson = buffer.toString();
                Log.d("Response", forcastJson);
                WeatherResponseAdapter weatherResponseAdapter = new WeatherResponseAdapter(forcastJson,Boolean.valueOf(params[1]));
                weatherResponseAdapter.parseResponse();
                //ArrayList<String> freshData = new ArrayList<String>();


            } catch (Exception e) {
                e.printStackTrace();
                Log.d("url connection error", "error");
            } finally {
                if (urlConnection != null)
                    urlConnection.disconnect();

                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException ex) {
                        Log.d("Buffer reader error", "Error");
                    }

                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            listForcast.setAdapter(recycleViewAdapter);
            swipeRefreshLayout.setRefreshing(false);

            ForecastBean forecastBean = ForecastBean.getInstance();

            beanArrayList = forecastBean.getForecastBeanArrayList();
            recycleViewAdapter = new RecycleViewAdapter(beanArrayList);

            listForcast.setAdapter(recycleViewAdapter);

            if(beanArrayList != null)
            {
                if(toFahrenheit)
                {
                    txtTodayTemp.setText(beanArrayList.get(0).getTempForcast()+DEGREE+"F");
                }
                else
                {
                    txtTodayTemp.setText(beanArrayList.get(0).getTempForcast()+DEGREE+"C");
                }

            }

        }

    }
    private class SwipeToRefresh implements SwipeRefreshLayout.OnRefreshListener {

        @Override
        public void onRefresh() {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(true);
                    swipeRefreshLayout.setEnabled(true);
                    new ForcastRequest().execute(city + ",in",String.valueOf(toFahrenheit));
                }
            });
            thread.start();
        }
    }
}
