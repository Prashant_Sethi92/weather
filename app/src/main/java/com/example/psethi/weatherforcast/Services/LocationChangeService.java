package com.example.psethi.weatherforcast.Services;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.example.psethi.weatherforcast.LocationHelper;
import com.example.psethi.weatherforcast.MainActivity;
import com.example.psethi.weatherforcast.R;
import com.example.psethi.weatherforcast.Utils.CreateNotification;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import static android.content.Context.LOCATION_SERVICE;
import static android.content.Context.MODE_PRIVATE;
import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by PSethi on 27-Oct-17.
 */

public class LocationChangeService extends Service {
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        SharedPreferences prefs = getSharedPreferences("APP Storage",MODE_PRIVATE);
                        String city = prefs.getString("city",null);

                        Log.d("LocationChangeService","app coming");
                        if(city != null)
                        {
                            String checkCity = LocationHelper.getCityLocation(LocationChangeService.this);
                            if(!city.equals(checkCity))
                            {
                                prefs.edit().putString("city",checkCity).commit();
                                new CreateNotification(LocationChangeService.this);
                            }
                        }
                    }
                });
            }
        }, 50000, 20000);
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
