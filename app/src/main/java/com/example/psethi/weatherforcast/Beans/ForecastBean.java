package com.example.psethi.weatherforcast.Beans;

import java.util.ArrayList;

/**
 * Created by PSethi on 17-Oct-17.
 */

public class ForecastBean {

    private static ForecastBean instance;
    private String tempForcast;
    private String min;
    private String max;
    private String pressure;
    private String humidy;
    private String weatherForcast;
    private String date;
    private ArrayList<ForecastBean> forecastBeanArrayList;

    public static ForecastBean getInstance()
    {
        if(instance == null)
        {
            instance = new ForecastBean();
        }
        return instance;
    }

    public String getTempForcast() {
        return tempForcast;
    }

    public void setTempForcast(String tempForcast) {
        this.tempForcast = tempForcast;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public String getMax() {
        return max;
    }

    public void setMax(String max) {
        this.max = max;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getHumidy() {
        return humidy;
    }

    public void setHumidy(String humidy) {
        this.humidy = humidy;
    }

    public String getWeatherForcast() {
        return weatherForcast;
    }

    public void setWeatherForcast(String weatherForcast) {
        this.weatherForcast = weatherForcast;
    }

    public ArrayList<ForecastBean> getForecastBeanArrayList() {
        return forecastBeanArrayList;
    }

    public void setForecastBeanArrayList(ArrayList<ForecastBean> forecastBeanArrayList) {
        this.forecastBeanArrayList = forecastBeanArrayList;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
