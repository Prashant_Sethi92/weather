package com.example.psethi.weatherforcast.Utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.example.psethi.weatherforcast.MainActivity;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by PSethi on 16-Apr-18.
 */

public class CreateNotification {

    Context context;
   public CreateNotification(Context context)
   {
       this.context = context;
       createNotification();
   }

    private void createNotification() {

        Intent intent = new Intent(context,MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context,1,intent,PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new Notification.Builder(context)
                .setContentTitle("Location changed")
                .setContentTitle("Temperature might have changed. Please check...")
                .setSmallIcon(android.R.drawable.btn_plus)
                .setContentIntent(pendingIntent).build();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0,notification);
    }
}
