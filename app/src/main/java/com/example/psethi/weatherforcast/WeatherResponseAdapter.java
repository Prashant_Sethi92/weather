package com.example.psethi.weatherforcast;

import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.psethi.weatherforcast.Beans.ForecastBean;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by PSethi on 05-Sep-16.
 */
public class WeatherResponseAdapter {

    private String response;
    private boolean toFahrenheit;
    public  WeatherResponseAdapter(String response,boolean toFahrenheit)
    {
        this.response = response;
        this.toFahrenheit = toFahrenheit;
    }

    public ArrayList<String> parseResponse()
    {
        ArrayList<String> temperature= new ArrayList<String>();
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray list = jsonObject.getJSONArray("list");

            ForecastBean forecastBean = ForecastBean.getInstance();
            ArrayList<ForecastBean> forecastBeanArrayList = new ArrayList<>();

            for(int i=0;i<list.length();i++)
            {
                ForecastBean bean = new ForecastBean();

                JSONObject object = list.getJSONObject(i);
                JSONObject tempObj = object.getJSONObject("temp");

                String temp = tempObj.getString("day");
                if(toFahrenheit)
                {
                    temp = convertToFahrenheit(temp);
                }else
                {
                    temp = convertToCelsius(temp);
                }


                bean.setTempForcast(temp);
                if(toFahrenheit)
                {
                    bean.setMax(convertToFahrenheit(tempObj.getString("max")));
                    bean.setMin(convertToFahrenheit(tempObj.getString("min")));
                }else
                {
                    bean.setMax(convertToCelsius(tempObj.getString("max")));
                    bean.setMin(convertToCelsius(tempObj.getString("min")));
                }


                String dateStr = object.getString("dt") + "000";
                long date = Long.valueOf(dateStr);

                bean.setPressure(object.getString("pressure"));
                bean.setHumidy(object.getString("humidity"));

                String dataToShow = null;

                dataToShow = new SimpleDateFormat("dd MMM yyyy").format(new Date(date));

                bean.setDate(dataToShow);

                JSONArray weather = object.getJSONArray("weather");
                JSONObject weatherObj = weather.getJSONObject(0);
                String forcast = weatherObj.getString("main");

                bean.setWeatherForcast(forcast);

                temperature.add(dataToShow);
                forecastBeanArrayList.add(bean);
            }
            forecastBean.setForecastBeanArrayList(forecastBeanArrayList);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return temperature;
    }

    private String convertToCelsius(String temp)
    {
        double tempInKelvin = Double.valueOf(temp);
        tempInKelvin = tempInKelvin - 273.15;
        temp = String.valueOf(Math.round(tempInKelvin));
        return temp;
    }

    private String convertToFahrenheit(String temp)
    {
        double tempInKelvin = Double.valueOf(temp);
        tempInKelvin = 9/5 *(tempInKelvin - 273) + 32;
        temp = String.valueOf(Math.round(tempInKelvin));
        return temp;
    }
}
