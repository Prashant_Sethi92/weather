package com.example.psethi.weatherforcast;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.psethi.weatherforcast.Beans.ForecastBean;

import java.util.ArrayList;

/**
 * Created by PSethi on 28-Aug-16.
 */
public class RecycleViewAdapter extends RecyclerView.Adapter<RecycleViewAdapter.CustomViewHolder> {

    ArrayList<ForecastBean> forecastBeanArrayList;

    public RecycleViewAdapter(ArrayList<ForecastBean> forecastBeanArrayList)
    {
        if(this.forecastBeanArrayList == null)
        {
            this.forecastBeanArrayList = new ArrayList<>();
        }
        this.forecastBeanArrayList = forecastBeanArrayList;
    }
    public static class CustomViewHolder extends RecyclerView.ViewHolder
    {
        public TextView dayTextview;
        public TextView minTempTextview;
        public TextView maxTempTextview;
        public CustomViewHolder(View v) {
            super(v);
            dayTextview = (TextView) v.findViewById(R.id.list_item_forcast_day_textview);
            minTempTextview = (TextView) v.findViewById(R.id.min_temp);
            maxTempTextview = (TextView) v.findViewById(R.id.max_temp);

        }
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_forcast,null,false);
        CustomViewHolder vh = new CustomViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        holder.dayTextview.setText(forecastBeanArrayList.get(position).getDate());
        holder.minTempTextview.setText("min: "+forecastBeanArrayList.get(position).getMin());
        holder.maxTempTextview.setText("max: "+forecastBeanArrayList.get(position).getMax());
    }

    @Override
    public int getItemCount() {
        if(forecastBeanArrayList != null)
        {
            return forecastBeanArrayList.size();
        }
        return 0;
    }
}
